# Purpose
This project is intended to promote [harmony](https://i1.wp.com/bodyharmonypt.com/wp-content/uploads/2016/02/BHPTYoga-e1455284578869.jpg?resize=1140%2C500) within the working group in terms of the nitty gritty details about model implementation and parameter choices that are made for common models.  This is done by using a validation of cross sections to ensure that the same (and reasonable) parameters are chosen consistently and in the end, a single set of cross sections is used commonly for the translation of limits on cross section to exclusions in the mass-mass plane.

# Conventions and Necessary Files
Each subdirectory here corresponds to a separate model.  In each case, there should be at least three files included in this directory

## README.txt 
This is a descriptive file where qualifications can be made with respect to the specific model in question.  An example is for the Spring 2017 MonoH results where ATLAS and CMS differed in the choices of parameters that were used.  This qualification is made in this (README.txt)[EW_Higgs_2HDM/README.txt].  However, if there are no qualifications, then this can simply be stated, and it is assumed that there is then only one agreed choice of parameters and cross sections, as specified in the `CrossSections_XXX.txt` and `run_card_XXX.dat` files.  This file should also contain the contact information for the relevant experts of this model who had input when arriving at these choices.

## CrossSections_XXX.txt 
This is the set of cross sections that are to be used commonly when making interpretation plots.  These should be arrived upon through the close working of a group of people from each experiment and a theorist if necessary.  What should be included in this file is (1) a header that specifies the location of the model used, the version of MadGraph, and the `run_card.dat` along with a newline delimited list where each line is a specific point in the parameter space of that model.  For the Zp2HDM model used by the MonoH searches, an example of what this file looks like is (found here)[EW_Higgs_2HDM/CrossSections_20170517_ATLASCMS_Run1Parameters_HiggsMassesFixedToMA.txt]:
```
########################################################################################
#UFO = https://gitlab.cern.ch/lhc-dmwg-material/model-repository/tree/master/models/EW_Higgs_2HDM
#run_card : https://gitlab.cern.ch/lhc-dmwg-material/cross_section-repository/tree/master/EW_Higgs_2HDM/run_card_20170517_ATLASCMS.dat
#MadGraph Version = MG5_v2_3_3
#mzp        ma         mhp        mh2        mDM        tan(beta)  gZ         sigma [pb]
########################################################################################
400        300        300        300        100        1.0        0.8        0.0191055000
400        400        400        400        100        1.0        0.8        0.0021354000
400        500        500        500        100        1.0        0.8        0.0006774600
400        600        600        600        100        1.0        0.8        0.0002911000
```
Specified on each line are *all of the model specific parameters that must be given to unambiguously specify the model*.

## run_card_XXX.dat 
This is the run_card that is used in the generation of the cross sections for a specific model.  This is needed to unambiguously define things like the PDF set used along with any generator level selections imposed on the cross section.  Each of the `run_card_XXX.dat` files contained here should be pionted at by a cross sections file.

## Extra Files
It is encouraged to include additional files or links to presentations where these models are studied, but this is not necessary.
